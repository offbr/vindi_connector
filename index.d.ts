declare namespace vindiConnector {
  export type Environment = "DEVELOPMENT" | "PRODUCTION";

  export function setToken(token: string): void;
  export function setEnvironment(environment: Environment): void;

  export function removePaymentProfile(id: number): any;
  export function getPaymentProfiles(customerID: number): any;
  export function addPaymentProfile(
    holder_name: string,
    card_expiration: string,
    card_number: number,
    card_cvv: number,
    payment_method_code: number,
    customer_id: number
  ): any;
  export function addPaymentProfile(
    holder_name: string,
    card_expiration: string,
    card_number: number,
    card_cvv: number,
    payment_method_code: number,
    customer_id: number,
    payment_company_code: string
  ): any;

  export function getBillsByDateRange(start_date: string, end_date: string, page?: number, per_page?: number): any;
  export function cancelBill(id: number): any;
  export function addBill(
    customer_id: number,
    product_id: number,
    code: string,
    payment_method_code: number,
    due_at: string,
    price: number,
    parcelas: number
  ): any;

  export function updateBill(id: number, data: any): any;

  export function getCustomer(external_code: string): any;
  export function addCustomer(
    name: string,
    cpf: string,
    email: string,
    matricula: string,
    street: string,
    number: number,
    complemento: string,
    cep: string,
    bairro: string,
    cidade: string,
    state: string,
    country: string,
    celular: string,
    telefone: string
  ): any;
  export function setCustomer(
    id: number,
    name: string,
    registry_code: string,
    email: string,
    code: string,
    street: string,
    number: number,
    complemento: string,
    cep: string,
    bairro: string,
    cidade: string,
    state: string,
    country: string,
    celular: string,
    telefone: string
  ): any;
  export function removeCustomer(id: number): any;

  export function getProduct(external_code: string): any;
  export function getPlan(external_code: string): any;
  export function getFaturaByCode(external_code: string): any;

  export function getSubscription(external_code: string): any;
  export function addSubscription(
    plan_id: number,
    customer_id: number,
    payment_method_code: number,
    product_id: number,
    start_at: string,
    price: number
  ): any;
  export function addSubscription(
    plan_id: number,
    customer_id: number,
    payment_method_code: number,
    product_id: number,
    start_at: string,
    price: number,
    code: string
  ): any;

  export function getSubscriptionItems(id: number): any;
  export function addDiscount(
    product_item_id: number,
    discount_type: string,
    percentage: number,
    amount: number,
    quantity: number,
    cycles: number | null
  ): any;

  export function changePaymentProfile(
    subscription_id: number,
    payment_profile_id: number
  ): any;

  export function cancelSubscription(external_code: string): any;

  export function verifyCard(profile_id: number): any;
  export function getFinalTrial(date: string, page: number): any;
  export function getNotification(id: number): any;
  export function getNotificationByName(name: string): any;
}

export = vindiConnector;
