const axios = require("axios");

exports.PRODUCTION = "https://app.vindi.com.br/api/v1";
exports.DEVELOPMENT = "https://sandbox-app.vindi.com.br/api/v1";

let apiURL = "";
let header = {};

exports.setToken = function (token) {
  header = {
    headers: {
      "Content-Type": "application/json",
    },
    auth: {
      username: token,
      password: "",
    },
  };
};

exports.setEnvironment = function (environment) {
  switch (environment) {
    case "DEVELOPMENT":
      apiURL = "https://sandbox-app.vindi.com.br/api/v1";
      break;
    case "PRODUCTION":
      apiURL = "https://app.vindi.com.br/api/v1";
      break;
    default:
      apiURL = "https://sandbox-app.vindi.com.br/api/v1";
  }
};

exports.removePaymentProfile = async function (id) {
  const response = await axios
    .delete(`${apiURL}/payment_profiles/${id}`, header)
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error.response.data;
    });

  return response;
};

exports.getPaymentProfiles = async function (customerID) {
  const response = await axios
    .get(
      `${apiURL}/payment_profiles?query=status=active customer_id=${customerID}`,
      header
    )
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error.response.data;
    });

  return response;
};

exports.addPaymentProfile = async function (
  holder_name,
  card_expiration,
  card_number,
  card_cvv,
  payment_method_code,
  customer_id,
  payment_company_code
) {
  const response = await axios
    .post(
      `${apiURL}/payment_profiles`,
      {
        holder_name,
        card_expiration,
        card_number,
        card_cvv,
        payment_method_code,
        payment_company_code,
        customer_id,
      },
      header
    )
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error.response.data;
    });

  return response;
};

exports.verifyCard = async function (profile_id) {
  const v_verify = await axios
    .post(`${apiURL}/payment_profiles/${profile_id}/verify`, {}, header)
    .then(function (response) {
      return response.data;
    })
    .catch((error) => {
      return error.response.data;
    });

  return v_verify;
};

exports.cancelBill = async function (id) {
  const response = await axios
    .delete(`${apiURL}/bills/${id}`, header)
    .then((resp) => {
      return resp.data;
    })
    .catch((error) => {
      return error.response.data;
    });

  return response;
};

exports.updateBill = async function (id, data) {
  const response = await axios
    .put(`${apiURL}/bills/${id}`, data, header)
    .then((resp) => {
      return resp.data;
    })
    .catch((error) => {
      return error.response.data;
    });

  return response;
};

exports.addBill = async function (
  customer_id,
  product_id,
  code,
  payment_method_code,
  due_at,
  price,
  parcelas
) {
  const response = await axios
    .post(
      `${apiURL}/bills`,
      {
        customer_id: customer_id,
        code: code,
        payment_method_code: payment_method_code,
        installments: parcelas,
        due_at: due_at,
        bill_items: [
          {
            product_id: product_id,
            amount: price,
          },
        ],
      },
      header
    )
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error.response.data;
    });

  return response;
};

exports.addCustomer = async function (
  name,
  cpf,
  email,
  matricula,
  street,
  number,
  complemento,
  cep,
  bairro,
  cidade,
  state,
  country,
  celular,
  telefone
) {
  let phones = Array();

  if (celular && celular.trim() > 0) {
    phones.push({
      phone_type: "mobile",
      number: celular,
    });
  }

  if (telefone && telefone.trim() > 0) {
    phones.push({
      phone_type: "landline",
      number: telefone,
    });
  }

  const response = await axios
    .post(
      `${apiURL}/customers`,
      {
        name: name,
        registry_code: cpf,
        email: email,
        code: matricula,
        notes: "ADICIONADO VIA MS FINANCEIRO",
        metadata: "metadata",
        address: {
          street: street,
          number: number,
          additional_details: complemento,
          zipcode: cep,
          neighborhood: bairro,
          city: cidade,
          state: state,
          country: country,
        },
        phones: phones,
      },
      header
    )
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error.response.data;
    });

  return response;
};

exports.setCustomer = async function (
  id,
  name,
  registry_code,
  email,
  code,
  street,
  number,
  complemento,
  cep,
  bairro,
  cidade,
  state,
  country,
  celular,
  telefone
) {
  let phones = Array();

  if (celular && celular.trim() > 0) {
    phones.push({
      phone_type: "mobile",
      number: celular,
    });
  }

  if (telefone && telefone.trim() > 0) {
    phones.push({
      phone_type: "landline",
      number: telefone,
    });
  }

  const response = await axios
    .put(
      `${apiURL}/customers/${id}`,
      {
        name,
        registry_code,
        email,
        code,
        notes: "ATUALIZADO VIA MS FINANCEIRO",
        address: {
          street,
          number,
          additional_details: complemento,
          zipcode: cep,
          neighborhood: bairro,
          city: cidade,
          state,
          country,
        },
        phones: phones,
      },
      header
    )
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error.response.data;
    });

  return response;
};

exports.getCustomer = async function (external_code) {
  const response = await axios
    .get(`${apiURL}/customers?query=code:${external_code}`, header)
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error.response.data;
    });

  return response;
};

exports.removeCustomer = async function (id) {
  const response = await axios
    .delete(`${apiURL}/subscriptions/${id}`, header)
    .then((resp) => {
      return resp.data;
    })
    .catch((error) => {
      return error.response.data;
    });

  return response;
};

exports.getFaturaByCode = async function (external_code) {
  const v_bill_info = await axios
    .get(`${apiURL}/bills?query=code=${external_code}`, header)
    .then(function (response) {
      return response.data;
    })
    .catch(() => {
      return [];
    });

  return v_bill_info;
};

exports.getProduct = async function (external_code) {
  const response = await axios
    .get(`${apiURL}/products?query=code:${external_code}`, header)
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error.response.data;
    });

  return response;
};

exports.getPlan = async function (external_code) {
  const response = await axios
    .get(`${apiURL}/plans?query=code:${external_code}`, header)
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error.response.data;
    });

  return response;
};

exports.getSubscriptionItems = async function (id) {
  const response = await axios
    .get(`${apiURL}/subscriptions/${id}/product_items`, header)
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error.response.data;
    });

  return response;
};

exports.addDiscount = async function (
  product_item_id,
  discount_type,
  percentage,
  amount,
  quantity,
  cycles
) {
  const response = await axios
    .post(
      `${apiURL}/discounts`,
      {
        product_item_id,
        discount_type,
        percentage,
        amount,
        quantity,
        cycles,
      },
      header
    )
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error.response.data;
    });

  return response;
};

exports.getSubscription = async function (external_code) {
  const response = await axios
    .get(`${apiURL}/subscriptions?query=code:${external_code}`, header)
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error.response.data;
    });

  return response;
};

exports.addSubscription = async (
  plan_id,
  customer_id,
  payment_method_code,
  product_id,
  start_at,
  price,
  code = ""
) => {
  const params = {
    start_at,
    plan_id,
    customer_id,
    code,
    payment_method_code,
    product_items: [
      {
        product_id,
        pricing_schema: {
          price,
        },
      },
    ],
  };

  const v_subscription = await axios
    .post(`${apiURL}/subscriptions`, params, header)
    .then(function (response) {
      return response.data;
    })
    .catch((error) => {
      return error.response.data;
    });

  return v_subscription;
};

exports.changePaymentProfile = async function (
  subscription_id,
  payment_profile_id
) {
  const params = {
    payment_profile: {
      id: payment_profile_id,
    },
  };

  const response = await axios
    .put(`${apiURL}/subscriptions/${subscription_id}`, params, header)
    .then((resp) => {
      return resp.data;
    })
    .catch((error) => {
      return error.response.data;
    });

  return response;
};

exports.cancelSubscription = async function (id) {
  const response = await axios
    .delete(`${apiURL}/subscriptions/${id}`, header)
    .then((resp) => {
      return resp.data;
    })
    .catch((error) => {
      return error.response.data;
    });

  return response;
};

exports.getNotification = async function (id) {
  const response = await axios
    .get(`${apiURL}/notifications/${id}`, header)
    .then((resp) => {
      return resp.data;
    })
    .catch((error) => {
      return error.response.data;
    });

  return response;
};

exports.getNotificationByName = async function (name) {
  const response = await axios
    .get(`${apiURL}/notifications`, header)
    .then(async (resp) => {
      const { notifications } = resp.data;
      let notification = [];

      await notifications.forEach((notifi) => {
        if (notifi.name === name) {
          notification = notifi;
        }
      });

      return notification;
    })
    .catch((error) => {
      return error.response.data;
    });

  return response;
};

exports.getFinalTrial = async function (date, page) {
  const response = await axios
    .get(
      `${apiURL}/subscriptions?query=(start_at=${date}T00:00:00.000-03:00 status:future)&per_page=50&page=${page}`,
      header
    )
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error.response.data;
    });

  return response;
};

exports.getBillsByDateRange = async function (start_date, end_date, page = 0, per_page = 50) {
  const response = await axios
    .get(
      `${apiURL}/bills?query=(created_at>=${start_date}T00:00:00.000-03:00 created_at<=${end_date}T23:59:59.000-03:00)&per_page=${per_page}&page=${page}`,
      header
    )
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      return error.response.data;
    });

    return response;
}